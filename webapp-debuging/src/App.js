import React, { useState } from "react";
import "./App.css";
import GitHubLogin from "./GithubLogin";

function App() {
  const [name, setName] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        {name && <h2>Hai {name}</h2>}
        <GitHubLogin
          // masukkan client id dan secret terlebih dahulu !!
          clientId="d88f525d0464adb3b021"
          clientSecret="c2ba25fd385605f45cbaab715070fe293af9d3db"
          redirectUri=""
          onSuccess={(name) => setName(name)}
          onFailure={(resp) => console.log(resp)}
        />
      </header>
    </div>
  );
}

export default App;
