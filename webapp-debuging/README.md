## Debuging Github Login

1. Membuat client id dan client secret untuk dimasukkan ke props component GithubLogin
2. terdapat error 404 saat pop up login, fix dengan mengubah utility toQuery `` let query = `${str}${key}==${params[key]}`; `` menjadi `` let query = `${str}${key}=${params[key]}`;``
3. memperbaiki fungsi requuest onGetAccestoken di body request code, dan response untuk dilanjutkan ke parameter fungsi onGetProfile.
4. ada trouble cors di localhost jadi saya menggunakan https://cors-anywhere.herokuapp.com/ sebagai proxying cors.
5. memperbaiki fungsi on get profile dan mengembalikan data pengguna yang sedang login dengan props on success ke komponen parent.
