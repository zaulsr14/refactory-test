let a = "4 9 7 5 8 9 3".split(" ");

let bubbleSort = (a) => {
  let len = a.length;
  let swapped;
  do {
    swapped = false;
    for (let i = 0; i < len; i++) {
      if (a[i] > a[i + 1]) {
        let tmp = a[i];
        a[i] = a[i + 1];
        a[i + 1] = tmp;
        console.warn(`${[a[i], a[i + 1]]} -> ${a}`);
        swapped = true;
      }
    }
  } while (swapped);
};
bubbleSort(a);
