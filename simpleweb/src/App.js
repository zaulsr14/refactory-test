import React from "react";
import "./App.css";

function App() {
  const [input, setInput] = React.useState("");
  const [history, setHistory] = React.useState([{ type: "patch", value: "" }]);

  const createHistory = (type, value) => {
    const newHistory = {
      type: type,
      value: value,
    };
    setHistory([...history, newHistory]);
  };

  const handleChange = (e) => {
    createHistory("patch", input);
    setInput(e.target.value);
  };

  const reverseInput = () => {
    const reversed = input.split("").reverse().join("");
    createHistory("reverse", input);
    setInput(reversed);
  };

  const undo = async () => {
    console.log("undo !");
    const oldHistory = history[history.length - 1];
    createHistory("undo", input);
    setInput(oldHistory.value);
  };

  const redo = () => {
    console.log("redo !");
    const undoHistory = history.filter((data) => data.type === "undo")[0];
    createHistory("redo", input);
    setInput(undoHistory.value);
  };

  return (
    <div className="main">
      <div className="container">
        <div className="form-group">
          <label>Input</label>
          <input
            type="text"
            className="form-control"
            onChange={handleChange}
            value={input}
          />
        </div>

        <div className="mt-3 text-center">
          <button className="btn btn-warning mr-2" onClick={reverseInput}>
            Reverse
          </button>
          <button
            className="btn btn-danger"
            onClick={undo}
            onDoubleClick={redo}
          >
            Undo / Redo
          </button>
        </div>
        <div className="mt-3 text-center text-white">
          <p className="m-0">1. Use single click to undo</p>
          <p className="m-0">2. Use Double Click click to Redo</p>
        </div>
      </div>
    </div>
  );
}

export default App;
